package com.example.demo.com.example.demo.service;

import com.example.demo.com.example.demo.dao.BrandsDao;
import com.example.demo.com.example.demo.dao.MeasureDao;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MeasureService {

    private final List<MeasureDao> measures;

    private HashSet<BrandsDao> brandsFilter;


    public MeasureService() {
        measures = loadMeasures();
        this.brandsFilter  = new HashSet<>();
    }

    private  List<MeasureDao> loadMeasures() {
        List<MeasureDao> m = new ArrayList<>();

        try {
            //BufferedReader br = new BufferedReader(new FileReader(ResourceUtils.getFile("classpath:data_measurements_finals.csv")));
            BufferedReader br = new BufferedReader(new InputStreamReader(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("data_measurements_finals.csv"))));
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String line = br.readLine();
            while ((line = br.readLine()) != null ){
                String[] measuresCsv = line.split(",");

                // create measure object to store values
                MeasureDao measure = new MeasureDao();

                // add values from csv to measure object
                measure.setId(measuresCsv[0] != null && !measuresCsv[0].isEmpty() ?  Integer.parseInt(measuresCsv[0]) : 0);
                measure.setTimestamp(measuresCsv[1] != null && !measuresCsv[1].isEmpty()  ? dateFormat.parse(measuresCsv[1]) : null );
                measure.setPress(measuresCsv[2] != null && !measuresCsv[2].isEmpty() && !measuresCsv[2].contains("-") ? new BigDecimal(measuresCsv[2]) : null);
                measure.setPosition(measuresCsv[3]);
                measure.setTemp(measuresCsv[4] != null && !measuresCsv[4].isEmpty()? Double.parseDouble(measuresCsv[4]) : 0);
                measure.setOmega(measuresCsv[5] != null && !measuresCsv[5].isEmpty() && !measuresCsv[5].contains("-") ? new BigDecimal(measuresCsv[5]) : new BigDecimal(0));
                measure.setSpeed(measuresCsv[6] != null && !measuresCsv[6].isEmpty() ? new BigDecimal(measuresCsv[6]) : null);
                measure.setCar_id(measuresCsv[7]);

                // adding measure objects to a list
                m.add(measure);
            }
        } catch (IOException | ParseException e) {
            Collections.sort(m);
            return m;
        }

        Collections.sort(m);
        return m;
    }
    public List<MeasureDao> getMeasures(int start, int end) {
        return  this.measures.subList(start, end);
    }

    public HashSet<BrandsDao> getBrands() {
        brandsFilter = (HashSet<BrandsDao>) this.measures.stream()
                .map(MeasureDao::getCar_id)
                .distinct()
                .map(BrandsDao::new)
                .collect(Collectors.toSet());

        return brandsFilter;
    }

    public List<MeasureDao> getFilteredMeasures(int from, int to) {

        List<String> acceptedBrands = brandsFilter.stream()
                                            .filter(BrandsDao::isPresent)
                                            .map(BrandsDao::getName)
                                            .collect(Collectors.toList());

        return  !acceptedBrands.isEmpty() ?
                this.measures.stream()
                    .filter(m -> acceptedBrands.contains(m.getCar_id()))
                    .collect(Collectors.toList())
                    .subList(from, to) :
                this.measures.subList(from,to);
    }

    public HashSet<BrandsDao> getUpdatedBrands(List<String> selectedBrands) {

        //update the brandsFilter
        brandsFilter.forEach(b ->
               b.setPresent(selectedBrands.contains(b.getName()))
        );

        return  brandsFilter;
    }
}
