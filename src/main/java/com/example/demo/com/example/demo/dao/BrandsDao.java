package com.example.demo.com.example.demo.dao;

import java.util.Objects;

public class BrandsDao {

    private String name;
    private Boolean isPresent;

    public BrandsDao(String name){
        this.name = name;
        this.isPresent = false;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isPresent() {
        return isPresent;
    }

    public void setPresent(Boolean present) {
        isPresent = present;
    }


}
