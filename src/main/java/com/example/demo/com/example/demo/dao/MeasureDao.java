package com.example.demo.com.example.demo.dao;

import java.math.BigDecimal;
import java.util.Date;

public class MeasureDao implements Comparable<MeasureDao>{
    public int id;
    public Date timestamp;
    public BigDecimal press;
    public String position;
    public double temp;
    public BigDecimal omega;
    public BigDecimal speed;
    public String car_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public BigDecimal getPress() {
        return press;
    }

    public void setPress(BigDecimal press) {
        this.press = press;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public BigDecimal getOmega() {
        return omega;
    }

    public void setOmega(BigDecimal omega) {
        this.omega = omega;
    }

    public BigDecimal getSpeed() {
        return speed;
    }

    public void setSpeed(BigDecimal speed) {
        this.speed = speed;
    }

    public String getCar_id() {
        return car_id;
    }

    public void setCar_id(String car_id) {
        this.car_id = car_id;
    }

    @Override
    public int compareTo(MeasureDao o) {
        return getTimestamp().compareTo(o.getTimestamp());
    }
}
