Documentazione della Soluzione
------
## 1. Approccio e Scelte Tecnologiche

   #### Persistenza dei Dati
   Per la persistenza dei dati, ho scelto di caricare il dataset in memoria al momento dell'avvio dell'applicazione, senza l'utilizzo di un database relazionale o NoSQL. Questa scelta è stata fatta considerando la dimensione limitata del dataset fornito e la natura simulata dello streaming di dati richiesto. Un'opzione più scalabile potrebbe essere l'utilizzo di un database relazionale o NoSQL per gestire grandi volumi di dati in tempo reale.

   #### Limitazioni della Soluzione
   Le principali limitazioni della soluzione attuale includono:
   * Dimensione del Dataset: La soluzione attuale carica l'intero dataset in memoria, il che potrebbe essere inefficiente per dataset molto grandi.
   * Assunzioni sui Dati: La soluzione assume che i dati nel file siano ben formattati e non contengano errori, a meno della richiesta di escludere valori negativi per "*pressione*" e "*velocità angolare*". Non c'è gestione degli errori per dati mancanti o malformati.

   #### Requisiti per la Scalabilità
   Per scalare la soluzione a milioni di pneumatici, è necessario considerare i seguenti miglioramenti:
  * Persistenza su Database: Utilizzare un database scalabile (ad esempio, MongoDB per dati non strutturati o PostgreSQL per dati strutturati) per gestire grandi volumi di dati in modo efficiente.
  * Parallelizzazione: Implementare la parallelizzazione delle operazioni per gestire simultaneamente flussi di dati da più sensori.
  * Distribuzione su Server: Distribuire l'applicazione su server multipli per bilanciare il carico e migliorare le prestazioni.
## 2. Struttura del Codice e Istruzioni per l'Installazione

   Il codice è organizzato in un progetto Spring Boot e l'utilizzo della libreria thymeleaf con due principali componenti:

+ Controller: La classe DemoController gestisce le richieste HTTP e interagisce con il servizio di misurazione.

+ Servizio: La classe MeasureService gestisce la logica di business, caricando i dati dal file CSV, filtrando e restituendo i dati richiesti.
+ Viste in HTMX: *Home* nella quale sono caricati i dati in card 10 alla volta, le card possono essere filtrate anche per car_id grazie ai check-box presenti nella pagina home, in base a quante tipologie di car_id vengono torvate nel file data_measurements_finals e a seguire l'ultima card c'è un pulsante che fa partire la chiamata /measurements che invocherà *MeasurementPages* che sostituirà l'ultima parte della pagina precedente con ulteriori 10 card
## 3.Istruzioni per l'Installazione

* Clonare il repository: git clone [link-del-repository]

* Navigare nella directory del progetto: cd [nome-del-progetto]

* Eseguire l'applicazione Spring Boot: ./mvnw spring-boot:run

* Aprire un browser e accedere a http://localhost:8080/home per visualizzare i dati iniziali.

Conclusioni
---
La soluzione attuale offre un'applicazione di base per la visualizzazione dei dati di telemetria. Per migliorare la scalabilità, è consigliabile migrare verso un sistema di persistenza dei dati più robusto e implementare strategie di gestione della concorrenza e del carico.

Nota: Assicurarsi di avere un ambiente Java e Maven configurato correttamente per eseguire l'applicazione. Ricorda di scaricare il csv al link https://www.dropbox.com/s/47wd4n6py9410kd/data_measurements.zip?dl=0 unzipparlo e salvarlo sotto src\main\resources
