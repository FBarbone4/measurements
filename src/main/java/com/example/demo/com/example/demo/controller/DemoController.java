package com.example.demo.com.example.demo.controller;

import com.example.demo.com.example.demo.service.MeasureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class DemoController {
    @Autowired
    private MeasureService service;

    private static final int PAGE_LENGHT = 10;
    @GetMapping("/home")
    public String getHome(Model model) {
        model.addAttribute("brands", service.getBrands());
        model.addAttribute("measurements", service.getMeasures(0, PAGE_LENGHT));
        return "home";
    }

    @GetMapping("/filterMeasurements")
    public String getFilteredMeasurements(@RequestParam(name = "selectedBrands", required = false) List<String> selectedBrands, Model model) {
        model.addAttribute("brands", service.getUpdatedBrands(selectedBrands));
        model.addAttribute("measurements", service.getFilteredMeasures(0, PAGE_LENGHT));
        return "home";
    }

    @GetMapping("/measurements")
    public String getMeasurementPages(@RequestParam("page")Integer page, Model model) {
        int to = page * PAGE_LENGHT;
        int from = to - PAGE_LENGHT;
        model.addAttribute("measurements", service.getFilteredMeasures(from, to));
        model.addAttribute("link", "/measurements?page=" + (page + 1));
        return "measurementPages";
    }
}
